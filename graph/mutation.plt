set terminal windows
set key below
set datafile separator ","
set tics font "Arial,10"
set key font "Arial,13"
plot [1:10000] [0:1] "mutationData.csv" using 1:2 with lines lw 5 title "average_a", "mutationData.csv" using 1:3 with line lw 5 title "min_a"